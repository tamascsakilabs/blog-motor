package configuration

import (
	"github.com/spf13/viper"
)

type AppConfig struct {
	LogLevel           string `mapstructure:"log_level"`
	LogFilePath        string `mapstructure:"log_file_path"`
	DBAdapter string `mapstructure:"db_adapter"`
	DBName string `mapstructure:"db_name"`
	JWTSecret string `mapstructure:"jwt_secret"`
	APPApiHost string `mapstructure:"app_api_host"`
}

func InitConfig() (AppConfig, error) {
	cfg := new(AppConfig)
	v := viper.New()

	v.SetDefault("LOG_FILE_PATH", "log/log.json")
	v.SetDefault("LOG_LEVEL", "info")
	v.SetDefault("DB_ADAPTER", "sqlite3")
	v.SetDefault("DB_NAME", "database.db")
	v.SetDefault("JWT_SECRET", "verysecret")
	v.SetDefault("APP_API_HOST", "http://localhost:8080")

	v.BindEnv("LOG_FILE_PATH")
	v.BindEnv("LOG_LEVEL")
	v.BindEnv("DB_ADAPTER")
	v.BindEnv("DB_NAME")
	v.BindEnv("JWT_SECRET")
	v.BindEnv("APP_API_HOST")

	if err := v.UnmarshalExact(cfg); err != nil {
		return *cfg, err
	}

	return *cfg, nil
}


