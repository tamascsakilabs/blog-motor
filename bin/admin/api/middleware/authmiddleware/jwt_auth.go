package authmiddleware

import (
	"errors"
	"net/http"

	"bitbucket.org/konstruktor/go-utils/jwtutils"
	"github.com/gin-gonic/gin"
)

type jwtAuthMiddleware struct {
	jwtSecret string
}

func NewJwtAuthMiddleware(secret string) *jwtAuthMiddleware {
	return &jwtAuthMiddleware{jwtSecret: secret}
}

func (m *jwtAuthMiddleware) Validate(c *gin.Context) {
	jwtValue := c.GetHeader("Token")
	if jwtValue == "" {
		c.AbortWithError(http.StatusUnauthorized, errors.New("missing value from Token header"))
		return
	}

	claims, err := jwtutils.DecodeClaims(m.jwtSecret, jwtValue)
	if err != nil {
		c.AbortWithError(http.StatusUnauthorized, err)
		return
	}

	claimUserID, ok := claims["user_id"]
	if !ok {
		c.AbortWithError(http.StatusUnauthorized, errors.New("invalid jwt token, user_id missing from payload"))
		return
	}

	userID, ok := claimUserID.(int64)
	if !ok {
		c.AbortWithError(http.StatusUnauthorized, errors.New("invalid jwt token, user_id not a number"))
		return
	}

	c.Set("user_id", int(userID))

	c.Next()
}
