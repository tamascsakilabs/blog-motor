package apimodels

type AddCommentRequest struct {
	PostID int64  `json:"postID"`
	Body   string `json:"body"`
}
