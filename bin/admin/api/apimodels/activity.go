package apimodels

type Activity struct {
	ID       int64
	Posts    int
	Comments int
}

type GetActivityResponse struct {
	Activities []Activity `json:"activities"`
}
