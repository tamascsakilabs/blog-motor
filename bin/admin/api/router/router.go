package router

import (
	"net/http"
	"strconv"

	"bitbucket.org/tamascsakilabs/blog-motor/bin/admin/api/apimodels"
	"github.com/gin-gonic/gin"
)

type AuthMiddleware interface {
	Validate(c *gin.Context)
}

type UserHandler interface {
	Login(request apimodels.LoginRequest) (response apimodels.LoginResponse, err error)
}

type CommentHandler interface {
	AddComment(userID int64, request apimodels.AddCommentRequest) (err error)
}

type ActivityHandler interface {
	GetActivity(days int) (response apimodels.GetActivityResponse, err error)
}

type Router struct {
	authMiddleware  AuthMiddleware
	userHandler     UserHandler
	commentHandler  CommentHandler
	activityHandler ActivityHandler
}

func NewRouter(authMiddleware AuthMiddleware, userHandler UserHandler, commentHandler CommentHandler, activityHandler ActivityHandler) *Router {
	return &Router{
		authMiddleware:  authMiddleware,
		userHandler:     userHandler,
		commentHandler:  commentHandler,
		activityHandler: activityHandler,
	}
}

func (r *Router) InitApi() *gin.Engine {
	apiRouter := gin.New()

	apiRouter.POST("/login", r.Login)
	apiRouter.GET("/add-comment", r.AddComment)
	apiRouter.GET("/activity/:days", r.GetActivity)

	return apiRouter
}

func (r *Router) Login(c *gin.Context) {
	var loginRequest apimodels.LoginRequest

	err := c.BindJSON(&loginRequest)
	if err != nil {
		return
	}

	response, err := r.userHandler.Login(loginRequest)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, response)
}

func (r *Router) AddComment(c *gin.Context) {
	userID := int64(c.GetInt("user_id"))

	var commentRequest apimodels.AddCommentRequest
	err := c.BindJSON(&commentRequest)
	if err != nil {
		return
	}

	err = r.commentHandler.AddComment(userID, commentRequest)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.Status(http.StatusOK)
}

func (r *Router) GetActivity(c *gin.Context) {
	daysParam := c.Param("days")
	days, err := strconv.Atoi(daysParam)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	response, err := r.activityHandler.GetActivity(days)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, response)
}
