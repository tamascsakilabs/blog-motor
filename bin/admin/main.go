package admin

import (
	"bitbucket.org/tamascsakilabs/blog-motor/bin/admin/api/middleware/authmiddleware"
	"bitbucket.org/tamascsakilabs/blog-motor/bin/admin/api/router"
	"bitbucket.org/tamascsakilabs/blog-motor/bin/admin/configuration"
	"bitbucket.org/tamascsakilabs/blog-motor/handlers/activityhandler"
	"bitbucket.org/tamascsakilabs/blog-motor/handlers/commenthandler"
	"bitbucket.org/tamascsakilabs/blog-motor/handlers/tokenhandler"
	"bitbucket.org/tamascsakilabs/blog-motor/handlers/userhandler"
	"bitbucket.org/tamascsakilabs/blog-motor/repository"
	"bitbucket.org/tamascsakilabs/blog-motor/repository/comment"
	"bitbucket.org/tamascsakilabs/blog-motor/repository/post"
	"bitbucket.org/tamascsakilabs/blog-motor/repository/user"
	"github.com/jinzhu/gorm"
)

func main() {
	appConfig, cfgErr := configuration.InitConfig()
	if cfgErr != nil {
		return
	}

	database, dbErr := repository.Connect(appConfig.DBName)
	if dbErr != nil {
		return
	}

	applicationRouter := InitServiceWithDependencies(database, appConfig)
	applicationRouter.InitApi().Run("8080")
}

func InitServiceWithDependencies(database *gorm.DB, appConfig configuration.AppConfig) *router.Router {
	userRepository := user.NewUserRepository(database)
	postRepository := post.NewPostRepository(database)
	commentRepository := comment.NewCommentRepository(database)

	tokenHandler := tokenhandler.NewTokenHandler(appConfig.JWTSecret)
	userHandler := userhandler.NewUserHandler(userRepository, tokenHandler)
	commentHandler := commenthandler.NewCommentHandler(postRepository, commentRepository)
	activityHandler := activityhandler.NewActivityHandler(userRepository, postRepository, commentRepository)

	authMiddleware := authmiddleware.NewJwtAuthMiddleware(appConfig.JWTSecret)

	return router.NewRouter(authMiddleware, userHandler, commentHandler, activityHandler)
}
