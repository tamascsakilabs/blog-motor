package commenthandler

import (
	"errors"

	"bitbucket.org/tamascsakilabs/blog-motor/bin/admin/api/apimodels"
)

type commentHandler struct {
	postRepository    PostRepository
	commentRepository CommentRepository
}

type PostRepository interface {
	GetUser(postID int64) (userID int64, err error)
}

type CommentRepository interface {
	AddComment(postID, userID int64, body string) (err error)
}

func NewCommentHandler(postRepository PostRepository, commentRepository CommentRepository) *commentHandler {
	return &commentHandler{
		postRepository:    postRepository,
		commentRepository: commentRepository,
	}
}

func (h *commentHandler) AddComment(userID int64, request apimodels.AddCommentRequest) (err error) {
	postID := request.PostID
	body := request.Body

	uploader, err := h.postRepository.GetUser(postID)
	if err != nil {
		return err
	}

	if userID != uploader {
		return errors.New("No access")
	}

	if len(body) < 1 {
		return errors.New("Empty body")
	}

	return h.commentRepository.AddComment(postID, userID, body)
}
