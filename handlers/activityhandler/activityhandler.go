package activityhandler

import (
	"bitbucket.org/tamascsakilabs/blog-motor/bin/admin/api/apimodels"
	"bitbucket.org/tamascsakilabs/blog-motor/model/post"
	"bitbucket.org/tamascsakilabs/blog-motor/model/comment"
	"bitbucket.org/tamascsakilabs/blog-motor/model/user"
)

type activityHandler struct {
	userRepository UserRepository
	postRepository PostRepository
	commentRepository CommentRepository
}

type UserRepository interface {
	GetAll() (users []user.User, err error)
}

type PostRepository interface {
	GetAllFromDateByUser(userID int64, days int) (posts []post.Post, err error)
}

type CommentRepository interface {
	GetAllFromDateByUser(userID int64, days int) (comments []comment.Comment, err error)
}

func NewActivityHandler(userRepository UserRepository, postRepository PostRepository, commentRepository CommentRepository) *activityHandler {
	return &activityHandler{
		userRepository: userRepository,
		postRepository: postRepository,
		commentRepository: commentRepository,
	}
}

func (h *activityHandler) GetActivity(days int) (response apimodels.GetActivityResponse, err error) {
	users, err := h.userRepository.GetAll()
	if err != nil {
		return response, err
	}

	var activities []apimodels.Activity
	for _, user := range users {
		userID := user.ID

		posts, err := h.postRepository.GetAllFromDateByUser(user.ID, days)
		if err != nil {
			return response, err
		}

		comments, err := h.commentRepository.GetAllFromDateByUser(user.ID, days)
		if err != nil {
			return response, err
		}

		activities = append(activities, apimodels.Activity{ID: userID, Posts: len(posts), Comments: len(comments)})
	}

	return apimodels.GetActivityResponse{Activities: activities}, nil
}
