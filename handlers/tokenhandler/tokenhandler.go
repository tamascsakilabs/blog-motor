package tokenhandler

import (
	"github.com/dgrijalva/jwt-go"
	"fmt"
	"errors"
)

type tokenHandler struct {
	jwtSecret string
}

func NewTokenHandler(secret string) *tokenHandler {
	return &tokenHandler{jwtSecret:secret}
}

func (t *tokenHandler) CreateToken(userID int64) (signedToken string, err error){
	token := jwt.New(jwt.SigningMethodHS512)
	claims := make(jwt.MapClaims)
	claims["id"] = userID
	token.Claims = claims
	return token.SignedString([]byte(t.jwtSecret))
}

func (t *tokenHandler) ValidateToken(tokenString string) (tokenData map[string]interface{}, err error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return tokenData, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(t.jwtSecret), nil
	})
	if err != nil {
		return nil, errors.New("Bad token")
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		_, ok := claims["id"]
		if !ok {
			return tokenData, errors.New("Bad token")
		}
		return claims, nil
	}
	return tokenData, errors.New("Bad token")
}
