package userhandler

import (
	"golang.org/x/crypto/bcrypt"
	"errors"
	"bitbucket.org/tamascsakilabs/blog-motor/bin/admin/api/apimodels"
	"bitbucket.org/tamascsakilabs/blog-motor/model/user"
)

type userHandler struct {
	userRepository UserRepository
	tokenHandler TokenHandler
}

type UserRepository interface {
	GetUserByEmail(email string) (user user.User, err error)
}

type TokenHandler interface {
	CreateToken(userID int64) (token string, err error)
}

func NewUserHandler(userRepository UserRepository, tokenHandler TokenHandler) *userHandler {
	return &userHandler{
		userRepository: userRepository,
		tokenHandler: tokenHandler,
	}
}

func (h *userHandler) Login(request apimodels.LoginRequest) (response apimodels.LoginResponse, err error) {
	email := request.Email
	password := request.Password

	user, err := h.userRepository.GetUserByEmail(email)
	if err != nil {
		return response, err
	}

	userID := user.ID
	hash := user.Password
	err = bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return response, errors.New("Bad password")
	}

	token, err := h.tokenHandler.CreateToken(userID)
	if err != nil {
		return response, err
	}

	response = apimodels.LoginResponse{
		Token: token,
	}
	return response, nil
}