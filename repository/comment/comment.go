package comment

import (
	"time"

	"github.com/jinzhu/gorm"
	commentmodel "bitbucket.org/tamascsakilabs/blog-motor/model/comment"
)

type commentRepository struct {
	db *gorm.DB
}

func NewCommentRepository(db *gorm.DB) *commentRepository {
	return &commentRepository{db: db}
}

func (r *commentRepository) AddComment(postID int64, userID int64, body string) error {
	currentDateTime := time.Now().Unix()

	comment := commentmodel.Comment{
		PostID:   postID,
		UserID:   userID,
		Body:     body,
		DateTime: currentDateTime,
	}

	return r.db.Create(&comment).Error
}

func (r *commentRepository) GetAllFromDateByUser(userID int64, days int) (comments []commentmodel.Comment, err error) {
	dateTime := time.Now().Unix() - int64(days*86400)
	err = r.db.Model(commentmodel.Comment{}).Where("comments.user_id = ? AND comments.date_time > ?", userID, dateTime).Find(&comments).Error
	return comments, err
}
