package user

import (
	"github.com/jinzhu/gorm"
	usermodel "bitbucket.org/tamascsakilabs/blog-motor/model/user"
)

type userRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *userRepository {
	return &userRepository{db: db}
}

func (r *userRepository) GetAll() (users []usermodel.User, err error) {
	err = r.db.Model(usermodel.User{}).Find(&users).Error
	return users, err
}

func (r *userRepository) GetUserByEmail(email string) (user usermodel.User, err error) {
	err = r.db.First(&user, "email = ?", email).Error
	return user, err
}
