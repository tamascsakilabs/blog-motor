package repository

import "github.com/jinzhu/gorm"

func Connect(dbName string) (db *gorm.DB, err error) {
	return gorm.Open("sqlite3", dbName)
}