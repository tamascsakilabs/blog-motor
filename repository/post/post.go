package post

import (

	"github.com/jinzhu/gorm"
	postmodel "bitbucket.org/tamascsakilabs/blog-motor/model/post"
	"time"
)

type postRepository struct {
	db *gorm.DB
}

func NewPostRepository(db *gorm.DB) *postRepository {
	return &postRepository{db: db}
}

func (r *postRepository) GetUser(postID int64) (userID int64, err error) {
	var post postmodel.Post
	err = r.db.First(&post, "id = ?", postID).Error
	return post.UserID, err
}

func (r *postRepository) GetAllFromDateByUser(userID int64, days int) (posts []postmodel.Post, err error) {
	dateTime := time.Now().Unix() - int64(days*86400)
	err = r.db.Model(postmodel.Post{}).Where("posts.user_id = ? AND posts.date_time > ?", userID, dateTime).Find(&posts).Error
	return posts, err
}