package comment

type Comment struct {
	ID       int64 `gorm:"PRIMARY_KEY"`
	PostID   int64
	UserID   int64
	Body     string
	DateTime int64
}

func (Comment) TableName() string {
	return "comments"
}