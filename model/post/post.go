package post

type Post struct {
	ID       int64 `gorm:"PRIMARY_KEY"`
	UserID   int64
	Title    string
	Body     string
	DateTime int64
}

func (Post) TableName() string {
	return "posts"
}