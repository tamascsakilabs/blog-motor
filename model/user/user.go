package user

type User struct {
	ID       int64 `gorm:"PRIMARY_KEY"`
	Email    string
	Password string
}

func (User) TableName() string {
	return "users"
}
